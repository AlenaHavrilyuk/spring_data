package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface RoleRepository extends JpaRepository<Role, UUID> {

  @Transactional
  @Modifying
  @Query(value = "Delete from roles r \n"
      + "where r.id not in ("
      + "select role_id from user2role)",
  nativeQuery = true)
  void deleteRole(String roleCode);
}
