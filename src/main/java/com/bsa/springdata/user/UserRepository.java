package com.bsa.springdata.user;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, UUID> {

  List<User> findByLastNameStartingWithIgnoreCase(String lastName, Pageable pageable);

  @Query("Select u from User u where u.office.city = :city order by u.lastName asc")
  List<User> findByCity(String city);

  @Query("Select u from User u where u.office.address = :address ")
  List<User> findByOfficeAddress(String address);

  List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

  @Query("Select u from User u where u.office.city = :city and u.team.room = :room order by u.lastName asc")
  List<User> findByRoomAndCity(String city, String room, Sort lastName);

  @Modifying
  @Query("Delete from User u where u.experience < :experience")
  int deleteByExperience(int experience);
}
