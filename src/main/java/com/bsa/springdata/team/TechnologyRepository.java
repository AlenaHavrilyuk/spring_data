package com.bsa.springdata.team;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
import org.springframework.data.jpa.repository.Query;

public interface TechnologyRepository extends JpaRepository<Technology, UUID> {

  Optional<Technology> findByName(String tech);
}
