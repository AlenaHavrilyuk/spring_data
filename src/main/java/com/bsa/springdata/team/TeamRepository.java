package com.bsa.springdata.team;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface TeamRepository extends JpaRepository<Team, UUID> {

  @Modifying
  @Transactional
  @Query(value = "update teams t \n"
      + "set name = (concat (t.name, '_', p.name, '_', tech.name))\n"
      + "from projects p, technologies tech\n"
      + "where t.name = :hipsters and t.technology_id = tech.id and t.project_id = p.id",
  nativeQuery = true)
  void normalizeName(String hipsters);

  Optional<Team> findByName(String name);

}
