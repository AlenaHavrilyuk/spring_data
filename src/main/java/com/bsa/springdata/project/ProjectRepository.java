package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Technology;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
import org.springframework.data.jpa.repository.Query;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

  @Query(value =
       "select p.id, p.name, p.description from projects p \n"
      + "join teams t on p.id = t.project_id\n"
      + "join technologies tech on tech.id = t.technology_id\n"
      + "join users u on u.team_id = t.id\n"
      + "where tech.name = :technology\n"
      + "group by p.id\n"
      + "order by count(*) desc\n"
      + "limit 5",
  nativeQuery = true)
  List<Project> findByTechnology(String technology);

  @Query(value = "select p.id, p.name, p.description from projects p \n"
      + "      join teams t on p.id = t.project_id\n"
      + "      join users u on u.team_id = t.id\n"
      + "      group by p.id\n"
      + "      order by count(distinct t.id) desc, count(*) desc, p.name desc\n"
      + "      limit 1",
  nativeQuery = true)
  Project findTheBiggest();

  @Query(value = "select p.name, count(distinct t.id) as TeamsNumber, count(u.id) as DevelopersNumber, \n"
      + "array_to_string(array_agg(distinct tech.name order by tech.name desc), ',') as Technologies\n"
      + "from projects p \n"
      + "join teams t on p.id = t.project_id\n"
      + "join technologies tech on tech.id = t.technology_id\n"
      + "join users u on u.team_id = t.id\n"
      + "group by p.name\n"
      + "order by p.name",
  nativeQuery = true)
  List<ProjectSummaryDto> getSummary();

  @Query(value = "select count(distinct p.id) from projects p \n"
      + "join teams t on p.id = t.project_id\n"
      + "join users u on u.team_id = t.id\n"
      + "join user2role u2r on u.id = u2r.user_id\n"
      + "join roles r on u2r.role_id = r.id\n"
      + "where r.name = :role",
  nativeQuery = true)
  int getCountProjectsWithRole(String role);
}