package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import com.bsa.springdata.user.dto.UserDto;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository
            .findByTechnology(technology)
            .stream()
            .map(ProjectDto:: fromEntity)
            .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        return Optional.of(ProjectDto.fromEntity(projectRepository.findTheBiggest()));
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.getSummary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.getCountProjectsWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        Technology technology = new Technology(createProjectRequest.getTech(),
            createProjectRequest.getTechDescription(),
            createProjectRequest.getTechLink());
        Team team = Team.builder()
            .name(createProjectRequest.getTeamName())
            .area(createProjectRequest.getTeamArea())
            .room(createProjectRequest.getTeamRoom())
            .build();
        Project project = new Project(
            createProjectRequest.getProjectName(),
            createProjectRequest.getProjectDescription());
        technologyRepository.saveAndFlush(technology);
        teamRepository.saveAndFlush(team);
        return projectRepository.saveAndFlush(project).getId();
    }
}
