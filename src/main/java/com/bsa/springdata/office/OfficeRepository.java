package com.bsa.springdata.office;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

  @Query("select distinct o from Office o \n"
      + "join o.users u\n"
      + "join u.team t\n"
      + "join t.technology tech\n"
      + "where tech.name = :technology")
  List<Office> getByTechnology(String technology);

  @Modifying
  @Transactional
  @Query(value = "update offices o \n"
      + "set address = :newAddress\n"
      + "from users u\n"
      + "where o.address = :oldAddress ",
  nativeQuery = true)
  void updateAddress(String oldAddress, String newAddress);

  Office getByAddress(String address);

}
