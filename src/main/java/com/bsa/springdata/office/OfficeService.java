package com.bsa.springdata.office;

import com.bsa.springdata.user.UserRepository;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {
    @Autowired
    private OfficeRepository officeRepository;
    @Autowired
    private UserRepository userRepository;

    public List<OfficeDto> getByTechnology(String technology) {
        return officeRepository.getByTechnology(technology)
            .stream()
            .map(OfficeDto::fromEntity)
            .collect(Collectors.toList());
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        if (userRepository.findByOfficeAddress(oldAddress).isEmpty()){
            return Optional.empty();
        }
        officeRepository.updateAddress(oldAddress,newAddress);
        return Optional.of(OfficeDto.fromEntity(officeRepository.getByAddress(newAddress)));
    }
}
